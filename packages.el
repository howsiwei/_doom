;; -*- no-byte-compile: t; -*-
;;; packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)

(disable-packages! ccls)
(disable-packages! ocamlformat utop)
(disable-packages! writegood-mode)

(package! solarized-theme)
(package! graphviz-dot-mode)
(package! rmsbolt)
(package! web-beautify)
(package! nov)
