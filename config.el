;;; config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!

(setq confirm-kill-emacs nil)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(set-popup-rule! "^\\*Process List\\*$"
  :vslot -6 :size #'+popup-shrink-to-fit :select t)

(setq doom-modeline-bar-width 1
      doom-modeline-buffer-file-name-style 'relative-from-project
      doom-modeline-buffer-encoding nil
      doom-modeline-env-version nil)

(load-theme 'solarized-light t)
(set-face-attribute 'region nil
                    :foreground nil
                    :distant-foreground nil
                    :background "#dbd8c9"
                    )
(set-face-attribute 'trailing-whitespace nil :background "#93a1a1")
(after! whitespace
  (set-face-attribute 'whitespace-tab nil :foreground "#93a1a1")
  )
(set-face-attribute 'line-number nil :background "#f5efdc")
(set-face-attribute 'line-number-current-line nil
                    :weight 'bold
                    :foreground "#657b83"
                    :background "#e5e1d2"
                    )
(set-face-attribute 'mode-line nil :underline nil)
(set-face-attribute 'mode-line-inactive nil :underline nil)
(set-face-attribute 'mode-line-emphasis nil :foreground "#268bd2")
(after! doom-modeline
  (set-face-attribute 'doom-modeline-bar-inactive nil :background "#fdf6e3")
  (set-face-attribute 'doom-modeline-panel nil :box nil)
  (set-face-attribute 'doom-modeline-evil-normal-state nil :foreground "#268bd2")
  )
(set-face-attribute '+workspace-tab-selected-face nil
                    :weight 'bold
                    :foreground "#586e75"
                    )
(set-face-attribute 'fixed-pitch nil :family 'unspecified)
;; Make org-level-2 to have size 15 instead of 14
(set-face-attribute 'variable-pitch nil :height 1.01)

(blink-cursor-mode 0)
(setq-hook! '(prog-mode-hook text-mode-hook conf-mode-hook)
  show-trailing-whitespace t)

(advice-add #'find-file :around #'doom-set-jump-a)
(add-hook! 'find-file-hook :append #'recentf-save-list)

(setq evil-move-cursor-back nil
      evil-move-beyond-eol t
      evil-kill-on-visual-paste nil
      evil-want-change-word-to-end nil)

(defun my--bol-bot (pt)
  (let* ((bol (save-excursion
                (goto-char pt)
                (line-beginning-position)))
         (bot (save-excursion
                (goto-char bol)
                (skip-chars-forward " \t\r")
                (point))))
    (list bol bot)))

(defun my--eol-eot (pt)
  (let* ((bol (save-excursion
                (goto-char pt)
                (line-beginning-position)))
         (eol (save-excursion
                (goto-char pt)
                (line-end-position)))
         (eot (if (not comment-use-syntax)
                  eol
                (save-excursion
                  (goto-char eol)
                  (while (and (doom-point-in-comment-p)
                              (> (point) bol))
                    (backward-char))
                  (skip-chars-backward " " bol)
                  (point)))))
    (list eol eot)))

(defun my-goto-indent-or-bol (&optional point)
  (interactive "^d")
  (let ((pt (or point (point))))
    (cl-destructuring-bind (bol bot)
        (my--bol-bot pt)
      (goto-char (if (= pt bot) bol bot)))))

(defun my-goto-eol-or-last-non-comment (&optional point)
  (interactive "^d")
  (let ((pt (or point (point))))
    (cl-destructuring-bind (eol eot) (my--eol-eot pt)
      (goto-char (if (= pt eol) eot eol)))))

(defun my-reload-buffer ()
  (interactive)
  (let* ((fl (buffer-file-name))
         (sw (selected-window))
         (ws (window-start))
         (pt (point)))
    (find-alternate-file fl)
    (set-window-start sw ws)
    (goto-char pt)))

;; copied from spacemacs/translate-C-i
(defun my-translate-C-i (_)
  "If the raw key sequence does not include <tab> or <kp-tab>, and
we are in the gui, translate to [C-i]. Otherwise, [9] (TAB)."
  (interactive)
  (if (and (not (cl-position 'tab (this-single-command-raw-keys)))
           (not (cl-position 'kp-tab (this-single-command-raw-keys)))
           (display-graphic-p))
      [C-i] [?\C-i]))
(define-key key-translation-map [?\C-i] #'my-translate-C-i)

;; https://stackoverflow.com/questions/22107182/in-emacs-flyspell-mode-how-to-add-new-word-to-dictionary
(defun my-save-word ()
  (interactive)
  (let ((current-location (point))
        (word (flyspell-get-word)))
    (when (consp word)
      (flyspell-do-correct 'save nil (car word) current-location (cadr word) (caddr word) current-location))))

(defun my-org-open-at-mouse-global (ev)
  "Open file link or URL at mouse.
See the docstring of `org-open-file' for details."
  (interactive "e")
  (mouse-set-point ev)
  (org-open-at-point-global)
  )

(setq doom-localleader-key ",")

(map! "C-a" #'my-goto-indent-or-bol
      "C-e" #'my-goto-eol-or-last-non-comment
      "C-j" #'evil-next-visual-line
      "C-k" #'evil-previous-visual-line
      "s-e" #'evil-escape
      "s-i" #'evil-escape
      "s-s" (lambda! (evil-force-normal-state) (save-buffer))
      [down-mouse-1] #'mouse-set-point
      [s-mouse-1] #'my-org-open-at-mouse-global

      :m "," nil
      :m ";" nil
      :m "C-e" nil
      :m [C-i] #'better-jumper-jump-forward
      :m "C-n" #'evil-scroll-line-down
      :m "C-p" #'evil-scroll-line-up
      :m "s-j" #'evil-scroll-page-down
      :m "s-k" #'evil-scroll-page-up

      :n "q" nil
      :n "x" nil
      :n "X" nil
      :n ";" #'pp-eval-expression
      :n "C-n" nil
      :n "C-p" nil
      :n "Q" #'evil-record-macro
      ;; "M-S-y" doesn't work
      :n "M-Y" #'evil-paste-pop-next
      :n "M-." (general-predicate-dispatch nil
                 (member last-command '(evil-repeat-pop evil-repeat-pop-next))
                 #'evil-repeat-pop-next)

      :nv "z z" #'my-save-word
      :nv "z x" #'flyspell-correct-at-point

      :i "C-a" nil
      :i "C-e" nil
      :i "C-j" nil
      :i "C-k" nil
      :i [S-return] #'+default/newline-above

      :leader
      :desc "Eval expression" ":" #'pp-eval-expression
      :desc "M-x" "x" #'execute-extended-command
      :desc "evilnc-comment-operator" ";" #'evilnc-comment-operator
      "/" #'+default/search-project
      "k" #'man-follow
      (:prefix "b"
       "R" #'my-reload-buffer)
      (:prefix "o"
       "p" #'list-processes
       :desc "Pop up scratch buffer" "x" #'doom/open-scratch-buffer
       :desc "Switch to scratch buffer" "X" #'doom/switch-to-scratch-buffer
       )
      (:prefix "TAB"
       "<" #'+workspace/swap-left
       ">" #'+workspace/swap-right
       )
      )

(map! :map evil-ex-completion-map
      "C-S-f" #'evil-ex-command-window
      )

(map! :map evil-ex-search-keymap
      "C-S-f" #'evil-ex-search-command-window
      )

(map! :map evil-window-map
      "x" #'ace-swap-window
      )

(map! :after dired
      :map dired-mode-map
      :n "RET" #'dired-find-alternate-file
      )

(after! flyspell
  (map! :map flyspell-mouse-map
        [mouse-1] #'flyspell-correct-word
        )
  )

(after! flycheck
  (set-popup-rule! "^\\*Flycheck error"
    :vslot -2 :size 0.25 :quit 'current :select t)
  (defun flycheck-hide-error-list-popup ()
    (interactive)
    (let ((window (flycheck-get-error-list-window)))
      (if window
          (+popup/close window t)))
    )
  (map! :map doom-leader-code-map
        "q" #'flycheck-hide-error-list-popup
        )
  )

;; command interpreter
(after! comint
  (setq comint-input-ring-size 10000)
  (setq-default comint-input-ignoredups t)
  )
(map! :after comint
      :map comint-mode-map
      :i "C-j" #'comint-next-matching-input-from-input
      :i "C-k" #'comint-previous-matching-input-from-input
      )

(after! shell
  (add-hook! 'shell-mode-hook
    (setq-local comint-input-ring-file-name "~/.local/share/zsh/zsh_history")
    ;; make timestamp optional to accommodate last line of history
    (setq-local comint-input-ring-separator "\n\\(: [0-9]+:[0-9]+;\\)?")
    (comint-read-input-ring)
    )
  )

(map! :map snippet-mode-map
      "C-c C-k" #'+snippet--abort
      )

(setq evil-snipe-override-evil-repeat-keys nil)
(after! evil-snipe
  (push 'ibuffer-mode evil-snipe-disabled-modes)
  )
(map! :after evil-snipe
      :map evil-snipe-override-local-mode-map
      :m "x" #'evil-snipe-repeat
      :m "X" #'evil-snipe-repeat-reverse
      :map evil-snipe-parent-transient-map
      ";" nil
      "," nil
      )

(after! markdown-mode
  ;; somehow js2-mode and rjsx-mode doesn't work
  (add-to-list 'markdown-code-lang-modes '("jsx" . js-mode))
  (set-face-attribute 'markdown-inline-code-face nil :foreground "#268bd2")
  (set-face-attribute 'markdown-code-face nil :foreground 'unspecified)
  (set-face-attribute 'markdown-header-face nil :weight 'bold)
  (set-face-attribute 'markdown-header-delimiter-face nil :weight 'bold)
  (set-face-attribute 'markdown-header-rule-face nil :weight 'bold)
  (setq markdown-toc-header-toc-start "<!-- table-of-contents-start -->"
        markdown-toc-header-toc-end "<!-- table-of-contents-end -->")
  (add-hook! 'markdown-mode-hook
    (add-hook! 'before-save-hook :local #'markdown-toc-refresh-toc)
    )
  (require 'toc-org)
  (advice-add #'toc-org-markdown-follow-thing-at-point :around #'doom-set-jump-a)
  (map! :map markdown-mode-mouse-map
        [remap markdown-follow-link-at-point] #'toc-org-markdown-follow-thing-at-point)
  (map! :map markdown-mode-map
        [remap markdown-follow-thing-at-point] #'toc-org-markdown-follow-thing-at-point)
  )

(defcustom my-document-directory "~/Documents"
  "Directory for storing technical documents."
  :type 'directory
  )

(setq org-directory "~/org"
      org-startup-folded nil
      org-agenda-files (list org-directory)
      )

(add-hook! 'org-load-hook :append
  (defun my-org-set-todo-keywords ()
    (setq org-todo-keywords
          '((sequence
             "[ ](t)"  ; A task that needs doing
             "[P](p)"  ; A project, which usually contains other tasks
             "[-](s)"  ; Task is in progress
             "[?](m)"  ; Task is still being considered
             "[W](w)"  ; Something external is holding up this task
             "[H](h)"  ; This task is paused/on hold because of me
             "|"
             "[X](d)"  ; Task successfully completed
             "[K](k)"  ; Task was cancelled, aborted or is no longer applicable
             )
            (sequence
             "TODO(T)" ; A task that needs doing & is ready to do
             "PROJ(P)" ; A project, which usually contains other tasks
             "STRT(S)" ; A task that is in progress
             "WAIT(W)" ; Something external is holding up this task
             "HOLD(H)" ; This task is paused/on hold because of me
             "|"
             "DONE(D)" ; Task successfully completed
             "KILL(K)" ; Task was cancelled, aborted or is no longer applicable
             )
            )
          org-todo-keyword-faces
          '(("[-]"  . +org-todo-active)
            ("STRT" . +org-todo-active)
            ("[?]"  . +org-todo-onhold)
            ("[W]"  . +org-todo-onhold)
            ("[H]"  . +org-todo-onhold)
            ("WAIT" . +org-todo-onhold)
            ("HOLD" . +org-todo-onhold)
            ("[P]"  . +org-todo-project)
            ("PROJ" . +org-todo-project)
            )
          )
    )
  )
(after! org
  (plist-put org-format-latex-options :scale 1.2)
  (set-face-attribute 'org-code nil :foreground "#268bd2")
  (set-face-attribute 'org-verbatim nil :foreground "#6c71c4")
  (+org-define-basic-link "document" 'my-document-directory)
  )

(after! evil-org
  (remove-hook 'org-tab-first-hook #'+org-cycle-only-current-subtree-h)
  )
(map! :after evil-org
      :map evil-org-mode-map
      :n "x" nil
      :n "X" nil
      :i "C-h" nil
      :i "C-l" nil
      )

(remove-hook 'org-load-hook #'org-roam-mode)
(after! org-roam (org-roam-mode))

(defun org-roam--extract-tags-description-list (_file)
  "Extract tags from description list."
  (cdr
   (org-element-map
       (org-element-parse-buffer 'greater-element t)
       org-element-all-elements
     (lambda (element)
       (let ((element-type (org-element-type element)))
         (cond ((member element-type '(section keyword)) nil)
               ((not (eq element-type 'plain-list)) '(nil))
               (t (cons
                   nil
                   (mapcan
                    (lambda (item-struct)
                      (if (equal (nth 5 item-struct) "tags")
                          (org-element-map
                              (org-element--parse-elements
                               (car item-struct) (car (last item-struct))
                               nil nil nil t nil)
                              'link
                            (lambda (link)
                              (substring-no-properties
                               (car (org-element-contents link)))))))
                    (org-element-property :structure element))))
               )))
     nil t))
  )

(defun my-org-roam-get-files-from-title (title)
  "Get files with the title."
  (let ((file-titles (org-roam-db-query [:select * :from titles])))
    (mapcar #'car (-filter (lambda (e) (member title (cadr e))) file-titles))
    )
  )

(setq org-roam-directory org-directory
      org-roam-capture-templates
      '(("d" "default" plain (function org-roam--capture-get-point)
         "%?"
         :file-name "${slug}"
         :head "#+TITLE: ${title}\n\n"
         :unnarrowed t)
        )
      org-roam-tag-sources '(description-list)
      +org-roam-open-buffer-on-find-file nil
      )
(map! :leader
      :prefix ("r" . "roam")
      :desc "Switch to buffer" "b" #'org-roam-switch-to-buffer
      :desc "Org Roam Capture" "c" #'org-roam-capture
      :desc "Find file"        "f" #'org-roam-find-file
      :desc "Find file"        "r" #'org-roam-find-file
      :desc "Show graph"       "g" #'org-roam-graph
      :desc "Insert"           "i" #'org-roam-insert
      :desc "Org Roam"         "o" #'org-roam
      (:prefix ("d" . "by date")
       :desc "Arbitrary date" "d" #'org-roam-dailies-date
       :desc "Today"          "t" #'org-roam-dailies-today
       :desc "Tomorrow"       "m" #'org-roam-dailies-tomorrow
       :desc "Yesterday"      "y" #'org-roam-dailies-yesterday
       )
      )
(after! org-roam
  (set-face-attribute 'org-roam-link nil :foreground "#2aa198")
  (set-face-attribute 'org-roam-link-current nil :foreground "#d33682")
  )

(defun my-org-journal-file-header-func (time)
  "Custom function to create journal header."
  (concat org-journal-date-prefix
          (format-time-string org-journal-date-format time)
          "\n\n- tags :: [[file:journal.org][Journal]], "
          (let ((day-of-week (format-time-string "%A" time)))
            (format "[[file:%s.org][%s]]" (downcase day-of-week) day-of-week))
          "\n\n* Tasks\n\n* Daily log\n"
          )
  )

(setq org-journal-dir org-directory
      org-journal-file-format "%Y-%m-%d.org"
      org-journal-file-header #'my-org-journal-file-header-func
      org-journal-date-prefix "#+TITLE: "
      org-journal-date-format "%Y-%m-%d"
      org-journal-time-prefix "** "
      org-journal-search-result-date-format "%A, %Y-%m-%d"
      )
(after! org-journal
  (map! :map org-journal-mode-map
        :localleader
        "d" nil
        "n" nil
        "p" #'org-priority
        )
  )

(after! osx-dictionary
  (set-popup-rule! "^\\*osx-dictionary\\*$"
    :size 0.25))

(after! recentf
  (setq recentf-max-saved-items 9000)
  )

(put 'undo-tree-auto-save-history 'safe-local-variable #'null)

(+global-word-wrap-mode +1)
(setq prettify-symbols-unprettify-at-point t)

(after! text-mode
  (remove-hook! 'text-mode-hook
    #'display-line-numbers-mode
    #'auto-fill-mode)
  )

(after! tex-mode
  (require 'math-symbol-lists)
  (setq +tex-prettify-symbols-alist tex--prettify-symbols-alist)
  (dolist (elem math-symbol-list-basic)
    (if (>= (length elem) 3)
        (add-to-list '+tex-prettify-symbols-alist
                     (cons (cadr elem) (caddr elem)))))
  (dolist (elem '(
                  ("\\begin" . ?▽)
                  ("\\end" . ?△)
                  ("\\(". ?⟅)
                  ("\\)". ?⟆)
                  ("\\{". ?⎨)
                  ("\\}". ?⎬)
                  ("\\bC" . ?ℂ)
                  ("\\bN" . ?ℕ)
                  ("\\bP" . ?ℙ)
                  ("\\bQ" . ?ℚ)
                  ("\\bR" . ?ℝ)
                  ("\\bZ" . ?ℤ)
                  ("\\implies" . ?⇒)
                  ("\\impliedby" . ?⇐)
                  ))
    (add-to-list '+tex-prettify-symbols-alist elem))
  (add-hook! 'TeX-mode-hook
    (setq prettify-symbols-alist +tex-prettify-symbols-alist)
    (prettify-symbols-mode)
    )
  )

(setq org-highlight-latex-and-related '(native))
(setq-default preview-scale 1.2)

(map! :after tex
      :map TeX-mode-map
      :localleader
      "a" #'TeX-command-run-all
      "c" #'TeX-command-master
      "r" (lambda! (TeX-region-update) (TeX-command "LatexMk" #'TeX-region-file))
      "," (lambda! (save-buffer) (TeX-command "LatexMk" #'TeX-master-file))
      )
(map! :after latex
      :map LaTeX-mode-map
      "C-j" nil
      )
(setq cdlatex-command-alist
      '(("dis" "Insert an DISPLAYMATH environment template"
         "" cdlatex-environment ("displaymath") t nil)
        ))
(map! :map cdlatex-mode-map
      :i "TAB" #'cdlatex-tab
      )

(after! smartparens-org
  (require 'smartparens-latex)
  (sp-with-modes '(org-mode)
    ;; (sp-local-pair "$" "$")
    (sp-local-pair "\\[" "\\]" :unless '(sp-latex-point-after-backslash))
    )
  )

(add-to-list 'auto-mode-alist '("\\.lagda\\.md\\'" . agda2-mode))
(add-to-list 'auto-mode-alist '("\\.lagda\\.org\\'" . agda2-mode))

(add-to-list 'auto-mode-alist '("\\(/\\|\\`\\)\\.clang-tidy\\'" . yaml-mode))

(after! cc-mode
  (setq flycheck-clang-pedantic t
        flycheck-gcc-pedantic t)
  (setq lsp-clients-clangd-args
        '("--completion-style=detailed"
          "--fallback-style=google"))
  (flycheck-remove-next-checker 'lsp 'c/c++-clang)
  (add-hook! '(c-mode-hook c++-mode-hook) #'rmsbolt-mode)
  )

(after! rmsbolt
  (setq rmsbolt-automatic-recompile nil)
  (set-popup-rule! "^\\*rmsbolt-output\\*$" :ignore t)
  )

(after! rustic
  (setq rustic-lsp-server 'rust-analyzer)
  )

(after! css-mode
  (setq css-indent-offset 2)
  )

(after! js
  (setq js-indent-level 2)
  )

(map! :after js2-mode
      :map js2-mode-map
      :localleader
      "x" #'rjsx-mode
      )

(after! web-mode
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-sql-indent-offset 2)
  )

(use-package! nov
  :mode ("\\.\\(epub\\|mobi\\)\\'" . nov-mode))

(setq nov-text-width t)
(add-hook! 'nov-mode-hook
  (face-remap-add-relative 'variable-pitch :height 1.2))
(add-hook 'nov-post-html-render-hook #'visual-line-mode)

(map! :map nov-mode-map
      :n "j" #'evil-next-visual-line
      :n "k" #'evil-previous-visual-line
      )
